TARGET = main
CC = g++
CFLAGS = -O3 -Wall -DNDEBUG
BUILD_CFLAGS = -fpic
OBJS = main.o
LIBS_INCLUDE = -I./utils/include
LIBS_DIR = -L./utils
LIBS = -lutils

all:
	@echo "Usage: $(MAKE) <platform>"
	@echo "  make linux"
	@echo "  make macosx"

.cpp.o:
	$(CC) -c $(CFLAGS) -o $@ $<

$(TARGET): $(OBJS)
	$(CC) $(LIBS_INCLUDE) $(LIBS_DIR) $(LIBS) -o $(TARGET) $(OBJS)

linux:
	@echo "========== make utis start =========="
	cd ./utils;make macosx
	cd ..
	@echo "========== make utis end =========="

macosx:
	@echo "========== make utis start =========="
	cd ./utils;make macosx
	cd ..
	@echo "========== make utis end =========="
	
	@echo "========== make main start =========="
	@$(MAKE) $(TARGET)
	@echo "========== make main end =========="

clean:
	@echo "========== clean utis start =========="
	cd ./utils;make clean
	cd ..
	@echo "========== clean utis end =========="
	
	@echo "========== clean main start =========="
	rm -f *.o *.so
	@echo "========== clean main end =========="


