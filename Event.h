//
// Created by GeLiusheng on 2021/2/25.
//

#ifndef CPP_SERVER_EVENT_H
#define CPP_SERVER_EVENT_H

#include <iostream>
#include <mutex>

class Event {
public:
    Event();
    void Wait();
    template <typename _Rep, typename _Period>
    bool WaitFor(const std::chrono::duration<_Rep, _Period> & duration);
    template <typename _Clock, typename _Duration>
    bool WaitUntil(const std::chrono::time_point<_Clock, _Duration> & point);
    void NotifyOne();
    void NotifyAll();
    void Reset();
private:
    Event(const Event &) = delete;
    Event & operator = (const Event &) = delete;
private:
    bool flag = false;
    bool all = false;

    std::mutex mutex_;
    std::condition_variable conVar_;
};


#endif //CPP_SERVER_EVENT_H
