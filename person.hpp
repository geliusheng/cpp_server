//
//  person.hpp
//  cpp_work
//
//  Created by GeLiusheng on 2021/2/15.
//  Copyright © 2021年 gels. All rights reserved.
//

#ifndef person_hpp
#define person_hpp

#include <stdio.h>

class Person {
private:
    char name[8];
    char sign[128];
    int age;
    
public:
    Person();
    ~Person();
    void setName(const char *);
    char* getName();
    void setAge(int nAge);
    int getAge();
    void print();
};

#endif /* person_hpp */
