//
// Created by GeLiusheng on 2021/3/20.
//

#include "mysqlclient.h"
#include <iostream>
#include <cassert>
#include <mysql.h>

MySQLClient::MySQLClient() {
    m_bConnected = false;
}

MySQLClient::~MySQLClient() {

}

bool MySQLClient::IsConnected() const {
    return m_bConnected;
}

bool MySQLClient::Connect(const char *host, const char *user, const char *pwd, unsigned int port, const char *db) {
    Disconnect();

    mysql_init(&m_hMysql);

    if (nullptr != mysql_real_connect(&m_hMysql, host, user, pwd, db, port, nullptr, 0)) {
        m_bConnected = true;
        m_szHost = host;
        m_szUser = user;
        m_szPwd = pwd;
        m_nPort = port;
        m_szDb = db;
    }else{
        mysql_close(&m_hMysql);
        std::cerr << "Failed to connect to mysql: " << mysql_error(&m_hMysql) << std::endl;
    }
    return m_bConnected;
}

bool MySQLClient::Reconnect() {
    Disconnect();

    if (!m_szHost.empty() && m_nPort > 0 && !m_szUser.empty() && !m_szPwd.empty() && !m_szDb.empty()) {
        return Connect(m_szHost.c_str(), m_szUser.c_str(), m_szPwd.c_str(), m_nPort, m_szDb.c_str());
    }
    return false;
}

void MySQLClient::Disconnect() {
    if (m_bConnected) {
        mysql_close(&m_hMysql);
        m_bConnected = false;
    }
}

bool MySQLClient::Query(const char *sql, ResultPointer *result, int *affected_rows) {
    if (!IsConnected() || sql == nullptr) {
        return false;
    }
    if (mysql_query(&m_hMysql, sql) == 0)
    {
        MYSQL_RES *db_result = mysql_store_result(&m_hMysql);
        if (db_result != nullptr && result != nullptr)
        {
            *result = std::make_unique<MySQLResult>(db_result);
        }
        if (affected_rows != nullptr)
        {
            *affected_rows = mysql_affected_rows(&m_hMysql);
        }
        return true;
    } else {
        std::cerr << "Failed to query database: " << mysql_error(&m_hMysql) << std::endl;
        return false;
    }
}






    /************************************************************************/

    MySQLResult::MySQLResult(MYSQL_RES *db_result)
            : result_(db_result)
    {
        assert(result_ != nullptr);
    }

    MySQLResult::~MySQLResult()
    {
        if (result_ != nullptr)
        {
            mysql_free_result(result_);
        }
    }

    // 行数
    my_ulonglong MySQLResult::num_rows() const
    {
        assert(result_ != nullptr);
        return result_ == nullptr ? 0 : mysql_num_rows(result_);
    }

    // 列数
    unsigned int MySQLResult::num_fields() const
    {
        assert(result_ != nullptr);
        return result_ == nullptr ? 0 : mysql_num_fields(result_);
    }

    // 取出所有列名
    std::vector<const char *> MySQLResult::fetch_field() const
    {
        assert(result_ != nullptr);
        std::vector<const char *> ret;
        if (result_ != nullptr)
        {
            MYSQL_FIELD *fd = nullptr;
            while ((fd = mysql_fetch_field(result_)) != nullptr)
            {
                ret.push_back(fd->name);
            }
        }
        return ret;
    }

    // 取出某行数据
    std::vector<const char *> MySQLResult::fetch_row(my_ulonglong row) const
    {
        assert(result_ != nullptr);
        std::vector<const char *> ret;
        if (result_ != nullptr)
        {
            ret.reserve(num_rows());
            my_ulonglong index = 0;
            MYSQL_ROW row_data = nullptr;
            for (index; (row_data = mysql_fetch_row(result_)) != nullptr; ++index)
            {
                if (index == row)
                {
                    for (unsigned int i = 0; i < num_fields(); ++i)
                    {
                        ret.push_back(row_data[i]);
                    }
                    break;
                }
            }
        }
        return ret;
    }
}
