/*
*本文件是mysql一些常用接口的封装
*时间：2021/3/20
*作者：葛流生
*/

#ifndef CPP_SERVER_MYSQLCLIENT_H
#define CPP_SERVER_MYSQLCLIENT_H

#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <memory>
#include <mysql.h>

class MySQLResult;
typedef std::unique_ptr<MySQLResult> ResultPointer;

/**
 * MySQL客户端包装类
 */
class MySQLClient
{
public:
    /**
     * 构造
     */
    MySQLClient();

    /**
     * 析构
     */
    ~MySQLClient();

    /**
     * 是否已连接
     */
    bool IsConnected() const;

    /**
     * 连接到数据库
     * @param host 主机地址
     * @param user 用户名
     * @param password 密码
     * @param db 数据库名
     * @param port 端口
     */
    bool Connect(const char *host, const char *user, const char *password, unsigned int port, const char *db);

    /**
     * 重新连接到数据库
     */
    bool Reconnect();

    /**
     * 断开连接
     */
    void Disconnect();

    /**
     * 执行查询操作
     * @param sql sql语句
     * @param result 查询结果
     * @param affected_rows 影响行数
     */
    bool Query(const char *sql, ResultPointer *result = nullptr, my_ulonglong *affected_rows = nullptr);

protected:
    MySQLClient(const MySQLClient &) = delete;
    MySQLClient& operator= (const MySQLClient &) = delete;

private:
    bool m_bConnected;
    MYSQL m_hMysql;
    std::string m_szHost;
    std::string m_szUser;
    std::string m_szPwd;
    unsigned int m_nPort;
    std::string m_szDb;
};

/**
 * MySQL查询返回结果包装类
 * @description 保证总能回收MYSQL_RES
 */
class MySQLResult
{
public:
    MySQLResult(MYSQL_RES *db_result);
    ~MySQLResult();

public:
    /**
     * 行数
     */
    my_ulonglong num_rows() const;

    /**
     * 列数
     */
    unsigned int num_fields() const;

    /**
     * 取出所有列名
     */
    std::vector<const char *> fetch_field() const;

    /**
     * 取出某行数据
     * @param row 行索引
     */
    std::vector<const char *> fetch_row(my_ulonglong row = 0) const;

protected:
    MySQLResult(const MySQLResult &) = delete;
    MySQLResult& operator= (const MySQLResult &) = delete;

private:
    MYSQL_RES*	m_hResult;
};

#endif //CPP_SERVER_MYSQLCLIENT_H
