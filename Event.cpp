//
// Created by GeLiusheng on 2021/2/25.
//

#include "Event.h"
#include <mutex>
#include <iostream>


Event::Event() {}
void Event::Wait()
{
    std::unique_lock<std::mutex> lock(mu);
    con.wait(lock, [this] () { return this->flag || this->all; });
    if (!all)
        flag = false;
}

template <typename _Rep, typename _Period>
bool Event::WaitFor(const std::chrono::duration<_Rep, _Period> & duration)
{
    std::unique_lock<std::mutex> lock(mu);
    bool ret = true;
    ret = con.wait_for(lock, duration, [this] () { return this->flag || this->all; });
    if (ret && !all)
        flag = false;
    return ret;
}

template <typename _Clock, typename _Duration>
bool Event::WaitUntil(const std::chrono::time_point<_Clock, _Duration> & point)
{
    std::unique_lock<std::mutex> lock(mu);
    bool ret = true;
    ret = con.wait_until(lock, point, [this] () { return this->flag || this->all; });
    if (ret && !all)
        flag = false;
    return ret;
}

void Event::NotifyOne()
{
    std::lock_guard<std::mutex> lock(mu);
    flag = true;
    con.notify_one();
}

void Event::NotifyAll()
{
    std::lock_guard<std::mutex> lock(mu);
    all = true;
    con.notify_all();
}

void Event::Reset()
{
    std::lock_guard<std::mutex> lock(mu);
    flag = all = false;
}
