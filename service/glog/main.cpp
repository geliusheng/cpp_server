//
// Created by GeLiusheng on 2021/2/24.
//
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <typeinfo>
#include <string.h>
#include "glog/logging.h"
#include "glog/stl_logging.h"
#include "utilities.h"

bool BoolFromEnv(const char *varname, bool defval) {
    const char* const valstr = getenv(varname);
    if (!valstr) {
        return defval;
    }
    return memchr("tTyY1\0", valstr[0], 6) != NULL;
}

int main(int argc, char* argv[]) {
    // Initialize Google's logging library.
    std::cout << "main enter" << std::endl;
    FLAGS_log_dir = "./aaa/";
//    // Set whether log messages go to stderr in addition to logfiles.
//    FLAGS_alsologtostderr = true;
//
//// Set color messages logged to stderr (if supported by terminal).
//    FLAGS_colorlogtostderr = true;
//
//// Set whether appending a timestamp to the log file name
//    FLAGS_timestamp_in_logfile_name = true;
//
//// Sets the maximum log file size (in MB).
//    FLAGS_max_log_size = 1024;
//
//// Use UTC time for logging
//    FLAGS_log_utc_time = true;

    google::InitGoogleLogging(argv[0]);

    DLOG(INFO) << "Hello, world!";
    std::cout << "main enter3  " << argv[0] << typeid(argv[0]).name() << "-----" << BoolFromEnv("GOOGLE_ALSOLOGTOSTDERR", false) << std::endl;

    // glog/stl_logging.h allows logging STL containers.
    std::vector<int> x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    LOG(INFO) << "ABC, it's easy as " << x;
    std::cout << "main enter4" << std::endl;

    return 0;
}