//
// Created by GeLiusheng on 2021/2/23.
//

#ifndef CPP_SERVER_SOCKETPOLL_H
#define CPP_SERVER_SOCKETPOLL_H

#include <stdbool.h>

typedef int poll_fd;

struct event {
    void * s;
    bool read;
    bool write;
    bool error;
    bool eof;
};

class SocketPoll {
public:
    virtual bool Invalid(int efd) = 0;
    virtual int Create() = 0;
    virtual void Release(int efd) = 0;
    virtual int Add(int efd, int sock, void *ud) = 0;
    virtual void Del(int efd, int sock) = 0;
    virtual int Enable(int efd, int sock, void *ud, bool read_enable, bool write_enable) = 0;
    virtual int Wait(int efd, struct event *e, int max) = 0;
    virtual void Nonblocking(int fd) = 0;
};










//
//bool sp_invalid(poll_fd fd);
//poll_fd sp_create();
//void sp_release(poll_fd fd);
//int sp_add(poll_fd fd, int sock, void *ud);
//void sp_del(poll_fd fd, int sock);
//int sp_enable(poll_fd, int sock, void *ud, bool read_enable, bool write_enable);
//int sp_wait(poll_fd, struct event *e, int max);
//void sp_nonblocking(int sock);
//
//#ifdef __linux__
//#include "socket_epoll.h"
//#endif
//
//#if defined(__APPLE__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined (__NetBSD__)
//#include "socket_kqueue.h"
//#endif


#endif //CPP_SERVER_SOCKETPOLL_H
