//
// Created by GeLiusheng on 2021/2/23.
//

#ifndef CPP_SERVER_SOCKETKQUEUE_H
#define CPP_SERVER_SOCKETKQUEUE_H

#include <iostream>
#include "SocketPoll.h"

class SocketKqueue : public SocketPoll {
public:
    bool Invalid(int efd);
    int Create();
    void Release(int efd);
    int Add(int efd, int sock, void *ud);
    void Del(int efd, int sock);
    int Enable(int efd, int sock, void *ud, bool read_enable, bool write_enable);
    int Wait(int efd, struct event *e, int max);
    void Nonblocking(int fd);
};


#endif //CPP_SERVER_SOCKETKQUEUE_H
