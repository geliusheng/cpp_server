//
// Created by GeLiusheng on 2021/2/24.
//

#include <stdio.h>
#include <iostream>
#include <queue>
#include <thread> //线程头文件
#include <mutex> //互斥锁头文件

std::condition_variable con; //条件变量, 通常和互斥锁一起使用
std::mutex mm; //互斥锁
std::queue<int> myqueue;
void threadFunc1 () {
    std::lock_guard<std::mutex> lock(mm); //自动释放锁
    //mm.lock();//加锁
    myqueue.push(std::rand());
    //mm.unlock(); //解锁
    con.notify_one(); //通知一个
}
void threadFunc2(){
    std::unique_lock<std::mutex> lock(mm); //unique_lock比lock_guard更灵活, 条件不满足的时候会自动释放锁
    while(myqueue.empty()) {
        con.wait(lock, [] () {return !myqueue.empty();}); //条件满足时获得锁, 编译选项-std=c++11
    }
    int num = myqueue.front();
    myqueue.pop();
    printf("==threadFunc2 print %d\n", num);
}
int main(int argc, char *argv[], char *evnp[]){
    std::thread *t1 = new std::thread(threadFunc1);
    std::thread *t2 = new std::thread(threadFunc2);
    t1->join(); //等待子线程完成
    t2->join(); //等待子线程完成
//    t1->detach(); //让子线程分离, 变成守护线程
//    t2->detach(); //让子线程分离, 变成守护线程
    delete t1;
    delete t2;
    return 0;
}
