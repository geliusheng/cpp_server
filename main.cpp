#include <stdio.h>
#include <iostream>
#include <string>

#define AA(x) num##x
#define BB(x) #x

int main() {
    class Test {
    public:
        static void f1(){
            std::cout << "Test f1" << std::endl;
        }
        static int f2(){
            Test::f1();
            std::cout << "Test f2" << std::endl;
            return 1;
        }
    };
    Test::f2();

    int num1 = 1000;
    std::string str = BB(888);
    std::cout << "Hello, World!" << "num1" << AA(1) << " " << str << std::endl;
    return 0;
}
