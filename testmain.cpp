//
// Created by GeLiusheng on 2021/3/23.
//

#include <iostream>
#include <vector>

template <typename T> class B;
class A {
public:
    A(){
        std::cout << "A construction" << std::endl;
    }
    ~A(){
        std::cout << "A destruction" << std::endl;
    }
    friend class B<int>;
};

template <typename TT>
class B {
private:
    TT m_t;
};

template <typename M = int, typename N = int>
auto addFunc(M m, N n) -> decltype (m + n) {
    return (m + n);
}

int main(int argv, char** argc) {
    std::unique_ptr<int> p;
    if (p == nullptr){
        std::cout <<"dddddddd=" << std::endl;
    }
    std::unique_ptr<A> ptr(new A());

    std::cout << "test template==" << addFunc(100, 200) << std::endl;
    std::string a = "1111";
    std::string b = "2222";
    auto c = addFunc(a, b);
    std::cout << "test template==" << c.c_str() << std::endl;

    std::vector<int> vec;
    for (int i = 1; i <= 10; ++i) {
        vec.push_back(i);
    }
    for (std::vector<int>::iterator ite = vec.begin(); ite != vec.end(); ite++) {
        std::cout << "==vector cell=" << *ite << std::endl;
    }
    for(auto ite = vec.begin(); ite != vec.end(); ite++) {
        std::cout << "==vector cell2=" << *ite << std::endl;
    }

    return 0;
}