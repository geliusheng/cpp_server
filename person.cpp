//
//  person.cpp
//  cpp_work
//
//  Created by GeLiusheng on 2021/2/15.
//  Copyright © 2021年 gels. All rights reserved.
//

#include "include/person.hpp"
#include <iostream>

Person::Person()
{
    memset(name, 0, sizeof(name));
    memset(sign, 0, sizeof(sign));
    this->age = 0;
}

Person::~Person()
{
    std::cout << "Person::~Person" << std::endl;
}

void Person::setName(const char * sName)
{
//    strncpy(name, sName, sizeof(this->name));
    strcpy(this->name, sName);
}

char* Person::getName()
{
    return name;
}

void Person::setAge(int nAge)
{
    this->age = nAge;
}

int Person::getAge()
{
    return age;
}

void Person::print()
{
    std::cout << "Person::print namesize=" << sizeof(this->name) << " ,name="<< name << " , age=" << this->getAge() << std::endl;
}
