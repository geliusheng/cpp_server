//
//  utils.hpp
//  cpp_work
//
//  Created by GeLiusheng on 2021/2/18.
//  Copyright © 2021年 gels. All rights reserved.
//

#ifndef utils_hpp
#define utils_hpp

#include <stdio.h>
#include <time.h>

class Utils {
private:
    
public:
    unsigned int getTime();
};
#endif /* utils_hpp */
