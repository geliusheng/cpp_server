//
//  utils.cpp
//  cpp_work
//
//  Created by GeLiusheng on 2021/2/18.
//  Copyright © 2021年 gels. All rights reserved.
//

#include "include/utils.hpp"
#include <stdio.h>
#include <iostream>
#include <time.h>

unsigned int Utils::getTime() {
    return (unsigned int) time(NULL);
}
